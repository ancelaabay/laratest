<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteController extends Controller
{
    
    public function sample()
    {  
        return view('layouts.admin.masterpage');
    }

    public function tables()
    {  
        return view('layouts.admin.tables');
    }

}
